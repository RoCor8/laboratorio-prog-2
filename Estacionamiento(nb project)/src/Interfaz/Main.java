/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import estacionamiento.Principal;
import estacionamiento.Estacionamiento;
import estacionamiento.Vehiculo;
import estacionamiento.Cliente;
import estacionamiento.Abonado;
import estacionamiento.Usuario;
import estacionamiento.Ticket;
import estacionamiento.Comprobante;
import java.util.Iterator;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Main extends javax.swing.JFrame {

    private Principal principal;
    private Vehiculo vehiculo;
    private Cliente cliente;
    private Abonado abonado;
    private Usuario usuario;
    private Ticket ticket;
    private Comprobante comprobante;
    private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
    
    

    public Main(Principal p){//solo dejar principal
        principal = p;
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Estacionamiento La Moto Feliz");
        int num=1;
        if (!principal.getVehiculos().isEmpty()) {
            if(principal.getVehiculos().size()>30){
                JOptionPane.showMessageDialog(null, "La playa está llena", "Atencion!", JOptionPane.QUESTION_MESSAGE);
            }else{
                for(Vehiculo veh : principal.getVehiculos()){
                    jTextAreaVehiculosEnPlaya.append(num+") Patente: "+veh.getPatente()+" - Marca: "+veh.getMarca()+" - Tipo: "+veh.getTipodeVehiculo()+" - Tarifa: $"+veh.getTarifa()+"\n");
                    num++;
                }
            }
            
        }
        else{
            jTextAreaVehiculosEnPlaya.append("No hay Vehiculos");
        }
    }
    
    
    public Main() {
        initComponents();
        }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaVehiculosEnPlaya = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuVehiculo = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        estacionarPrimera = new javax.swing.JMenuItem();
        estacionarRegular = new javax.swing.JMenuItem();
        estacionarAbonado = new javax.swing.JMenuItem();
        salidaVehiculo = new javax.swing.JMenuItem();
        menuAbono = new javax.swing.JMenu();
        nuevoAbono = new javax.swing.JMenuItem();
        añadirSaldo = new javax.swing.JMenuItem();
        añadirVehiculo = new javax.swing.JMenuItem();
        listaAbonados = new javax.swing.JMenuItem();
        menuGestion = new javax.swing.JMenu();
        verTicket = new javax.swing.JMenuItem();
        caja = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jTextAreaVehiculosEnPlaya.setColumns(20);
        jTextAreaVehiculosEnPlaya.setRows(5);
        jScrollPane1.setViewportView(jTextAreaVehiculosEnPlaya);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cabrioletred.png"))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/motorcycle.png"))); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/generatedtext.png"))); // NOI18N

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/generatedtext(1).png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel4))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3))))
                .addContainerGap(47, Short.MAX_VALUE))
        );

        menuVehiculo.setText("Vehiculos");

        jMenu3.setText("Estacionar Vehiculo");

        estacionarPrimera.setText("Primera Vez");
        estacionarPrimera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estacionarPrimeraActionPerformed(evt);
            }
        });
        jMenu3.add(estacionarPrimera);

        estacionarRegular.setText("Regular");
        estacionarRegular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estacionarRegularActionPerformed(evt);
            }
        });
        jMenu3.add(estacionarRegular);

        estacionarAbonado.setText("Abonado");
        estacionarAbonado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estacionarAbonadoActionPerformed(evt);
            }
        });
        jMenu3.add(estacionarAbonado);

        menuVehiculo.add(jMenu3);

        salidaVehiculo.setText("Salida Vehiculo");
        salidaVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salidaVehiculoActionPerformed(evt);
            }
        });
        menuVehiculo.add(salidaVehiculo);

        jMenuBar1.add(menuVehiculo);

        menuAbono.setText("Abono");

        nuevoAbono.setText("Nuevo Abono");
        nuevoAbono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoAbonoActionPerformed(evt);
            }
        });
        menuAbono.add(nuevoAbono);

        añadirSaldo.setText("Añadir Saldo");
        añadirSaldo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                añadirSaldoActionPerformed(evt);
            }
        });
        menuAbono.add(añadirSaldo);

        añadirVehiculo.setText("Añadir Vehiculo");
        añadirVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                añadirVehiculoActionPerformed(evt);
            }
        });
        menuAbono.add(añadirVehiculo);

        listaAbonados.setText("Lista de Abonados");
        listaAbonados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaAbonadosActionPerformed(evt);
            }
        });
        menuAbono.add(listaAbonados);

        jMenuBar1.add(menuAbono);

        menuGestion.setText("Gestion");

        verTicket.setText("Ver Tickets");
        verTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verTicketActionPerformed(evt);
            }
        });
        menuGestion.add(verTicket);

        caja.setText("Caja");
        caja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaActionPerformed(evt);
            }
        });
        menuGestion.add(caja);

        jMenuBar1.add(menuGestion);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void estacionarPrimeraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estacionarPrimeraActionPerformed
        // TODO add your handling code here:
        EntradaVehiculoNuevo entrada= new EntradaVehiculoNuevo(principal, cliente, vehiculo);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_estacionarPrimeraActionPerformed

    private void añadirVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_añadirVehiculoActionPerformed
        // TODO add your handling code here:
        AbonoAddVehiculo entrada= new AbonoAddVehiculo(principal, abonado);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_añadirVehiculoActionPerformed

    private void nuevoAbonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevoAbonoActionPerformed
        // TODO add your handling code here:
        AbonoNuevo entrada= new AbonoNuevo(principal, abonado);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_nuevoAbonoActionPerformed

    private void estacionarRegularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estacionarRegularActionPerformed
        // TODO add your handling code here:
        EntradaVehiculoRegular entrada= new EntradaVehiculoRegular(principal, cliente);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
    }//GEN-LAST:event_estacionarRegularActionPerformed

    private void estacionarAbonadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estacionarAbonadoActionPerformed
        // TODO add your handling code here:
        EntradaVehiculoAbonado entrada= new EntradaVehiculoAbonado(principal, abonado);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_estacionarAbonadoActionPerformed

    private void salidaVehiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salidaVehiculoActionPerformed
        // TODO add your handling code here:
        SalidaVehiculo entrada= new SalidaVehiculo(principal, cliente);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_salidaVehiculoActionPerformed

    private void añadirSaldoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_añadirSaldoActionPerformed
        // TODO add your handling code here:
        AbonoAddSaldo entrada= new AbonoAddSaldo(principal, abonado);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
    }//GEN-LAST:event_añadirSaldoActionPerformed

    private void listaAbonadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaAbonadosActionPerformed
         ListaAbonados entrada= new ListaAbonados(principal);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);
        dispose();
    }//GEN-LAST:event_listaAbonadosActionPerformed

    private void verTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verTicketActionPerformed
        // TODO add your handling code here:
        ListaTickets tickets=new ListaTickets(principal);
        tickets.setLocationRelativeTo(null);
        tickets.setVisible(true);
        dispose();
    }//GEN-LAST:event_verTicketActionPerformed

    private void cajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaActionPerformed
        // TODO add your handling code here:
        Caja caja=new Caja(principal);
        caja.setVisible(true);
        dispose();
    }//GEN-LAST:event_cajaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem añadirSaldo;
    private javax.swing.JMenuItem añadirVehiculo;
    private javax.swing.JMenuItem caja;
    private javax.swing.JMenuItem estacionarAbonado;
    private javax.swing.JMenuItem estacionarPrimera;
    private javax.swing.JMenuItem estacionarRegular;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaVehiculosEnPlaya;
    private javax.swing.JMenuItem listaAbonados;
    private javax.swing.JMenu menuAbono;
    private javax.swing.JMenu menuGestion;
    private javax.swing.JMenu menuVehiculo;
    private javax.swing.JMenuItem nuevoAbono;
    private javax.swing.JMenuItem salidaVehiculo;
    private javax.swing.JMenuItem verTicket;
    // End of variables declaration//GEN-END:variables
}
