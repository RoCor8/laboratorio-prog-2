
package estacionamiento;

import Exceptions.AbonadoNoEncontradoException;
import Exceptions.PlayaDeEstacionamientoLlenaException;
import Exceptions.VehiculoNoEncontradoException;
import Exceptions.VerficarCampoVacioException;
import Exceptions.VerificarDniException;
import Exceptions.VerificarRepetidosException;
import java.util.ArrayList;


public class Principal {//Esto controla todos los datos principales
    private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<Abonado> abonado=new ArrayList<Abonado>();
    private ArrayList<Vehiculo> totales=new ArrayList<Vehiculo>();
    private ArrayList<Comprobante> comprobantes=new ArrayList<Comprobante>();

    public ArrayList<Comprobante> getComprobantes() {
        return comprobantes;
    }

    public void setComprobantes(Comprobante c) {
        comprobantes.add(c);
    }
    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ArrayList<Vehiculo> getTotales() {
        return totales;
    }

    public void setTotales(Vehiculo t) {
        totales.add(t);
    }
    
    public ArrayList<Abonado> getAbonado() {
        return abonado;
    }

    public void setAbonado(Abonado a) {
        abonado.add(a);
    }

    
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Vehiculo v) throws PlayaDeEstacionamientoLlenaException{
        if (this.getVehiculos().size()<30){
            vehiculos.add(v);
        }else{
            throw new PlayaDeEstacionamientoLlenaException();
        }
        
    }
    
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Cliente c) {
        clientes.add(c);
    }
    
    public void verificarCampoVacio(String campo)throws VerficarCampoVacioException{
        if (campo.equals("")) {
            throw new VerficarCampoVacioException();
        }
    }
    
    public void verificarCampoVacio(String campo1, String campo2)throws VerficarCampoVacioException{
        if (campo1.equals("") || campo2.equals("")) {
            throw new VerficarCampoVacioException();
        }
    }
    
    public void verificarDatos(String dni, String nombre, String apellido) throws VerificarDniException, VerficarCampoVacioException {

        if (dni.length() == 8 || dni.length() == 7) {
            int numero = Integer.parseInt(dni);
            if (numero < 1000000) {
                throw new VerificarDniException();
            }
        } else {
            throw new VerificarDniException();
        }

        if (nombre.equals("") || apellido.equals("")) {
            throw new VerficarCampoVacioException();
        }

    }
  
    public void buscarRepetido(String dni) throws VerificarRepetidosException {//si se repite algun afiliado
        for (Cliente cli : this.getClientes()) {
            if (cli.getDocumento().equals(dni)) {
                throw new VerificarRepetidosException();
            }
        }
    }
    
    public void verificarExistenciaVehiculo(String matricula) throws VehiculoNoEncontradoException{
        int bandera = 0;
        for (Vehiculo vehi : this.getVehiculos()) {
            if (vehi.getPatente().equals(matricula)) {
                bandera = 1;
                break;
            }
        }
        if (bandera == 0) {
            throw new VehiculoNoEncontradoException();
        }
    }
    
    public void verificarDni(String dni) throws VerificarDniException {//deberia ir en UI. Puede ser static

        if (dni.length() == 8 || dni.length() == 7) {
            int numero = Integer.parseInt(dni);
            if (numero < 1000000) {
                throw new VerificarDniException();
            }
        } else {
            throw new VerificarDniException();
        }
        
    }
    
    public void verificarExistenciaAbonado(String dni) throws AbonadoNoEncontradoException {
        int bandera = 0;
        for (Abonado ab : this.getAbonado()) {
            if (ab.getDocumento().equals(dni)) {
                bandera = 1;
                break;
            }
        }
        if (bandera == 0) {
            throw new AbonadoNoEncontradoException();
        }
    }
    
    public Vehiculo buscarVehiculoEnPlaya(String matricula) throws VehiculoNoEncontradoException{
        Vehiculo vehi=null;
        int bandera=0;
        for (Vehiculo ve : this.getVehiculos()){
            if(ve.getPatente().equals(matricula)){
                vehi=ve;
                bandera=1;
                break;
            }
        }
        if (bandera==1){
            return vehi;
        }
        else{
            throw new VehiculoNoEncontradoException();
        }
    }
    
    public Vehiculo buscarVehiculoEnTotales(String matricula) throws VehiculoNoEncontradoException{
        Vehiculo vehi=null;
        int bandera=0;
        for (Vehiculo ve : this.getTotales()){
            if(ve.getPatente().equals(matricula)){
                vehi=ve;
                bandera=1;
                break;
            }
        }
        if (bandera==1){
            return vehi;
        }
        else{
            throw new VehiculoNoEncontradoException();
        }
    }
    
    public Abonado buscarAbonados(String dni) throws AbonadoNoEncontradoException{
        Abonado ab=null;
        int bandera=0;
        for (Abonado abo : this.getAbonado()){
            if(abo.getDocumento().equals(dni)){
                ab=abo;
                bandera=1;
                break;
            }
        }
        if (bandera==1){
            return ab;
        }
        else{
            throw new AbonadoNoEncontradoException();
        }
    }
}
