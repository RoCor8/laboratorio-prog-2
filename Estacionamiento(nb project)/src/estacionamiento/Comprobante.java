/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamiento;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class Comprobante {
    private Cliente cliente;
    private String matriculaVehiculo;
    private Integer montoCobrado;
    private String fechaDeIngreso;
    private String horaDeIngreso;
    private Integer ingreso;
    private String usuario;


    public Comprobante(Cliente cliente, String matriculaVehiculo , Integer montoCobrado , String fechaDeIngreso , String horaDeIngreso, Integer ingreso, String usuario)
    {
        this.cliente=cliente;
        this.matriculaVehiculo=matriculaVehiculo;
        this.montoCobrado=montoCobrado;
        this.fechaDeIngreso=fechaDeIngreso;
        this.horaDeIngreso=horaDeIngreso;
        this.ingreso=ingreso;
        this.usuario=usuario;
    }

    public String getMatriculaVehiculo() {
        return matriculaVehiculo;
    }

    public void setMatriculaVehiculo(String matriculaVehiculo) {
        this.matriculaVehiculo = matriculaVehiculo;
    }
    
    public Integer getIngreso() {
        return ingreso;
    }

    public void setIngreso(Integer Ingreso) {
        this.ingreso = Ingreso;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public String getHoraDeIngreso() {
        return horaDeIngreso;
    }

    public void setHoraDeIngreso(String horaDeIngreso) {
        this.horaDeIngreso = horaDeIngreso;
    }

    public String getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(String fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }

    public Integer getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Integer montoCobrado) {//debe verificar si el cliente tiene abono. En caso de tenerlo, debe llamar al metodo debitar de Abono
        this.montoCobrado = montoCobrado;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
}
