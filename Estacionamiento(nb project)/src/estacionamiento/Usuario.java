/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamiento;


public class Usuario extends Persona {
    private String identificacion;
    private Integer caja;

    public Usuario(String nombre, String documento, String apellido, String identificacion, Integer caja) {
        super(nombre, documento, apellido);
        this.identificacion=identificacion;
        this.caja=caja;
    }

    public Integer getCaja() {
        return caja;
    }

    public void setCaja(Integer caja) {
        this.caja = caja+caja;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}
