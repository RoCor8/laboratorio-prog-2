/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamiento;


public class Vehiculo {
    private String patente;
    private String marca;
    private String tipoDeVehiculo;
    private int tarifa;
    private Integer ingreso;
    
    /*public Vehiculo(Integer ingreso){
        this.ingreso=ingreso;
    }*/

    public Vehiculo(){}
    
    public Integer getIngreso() {
        return ingreso;
    }

    public void setIngreso(Integer ingreso) {
        this.ingreso = ingreso;
    }
    
    public Vehiculo(String patente,String marca,String tipoDeVehiculo, Integer tarifa){
        this.patente=patente;
        this.marca=marca;
        this.tipoDeVehiculo=tipoDeVehiculo;
        this.tarifa=tarifa;
    }

    public String getTipoDeVehiculo() {
        return tipoDeVehiculo;
    }

    public void setTipoDeVehiculo(String tipoDeVehiculo) {
        this.tipoDeVehiculo = tipoDeVehiculo;
    }

    public Integer getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        if (ingreso>1){
            this.tarifa=0;
        }
        else{
            this.tarifa = tarifa;    
        }
        
    }

    
    
    public void setPatente(String patente){
        this.patente=patente;
    }
    
    public String getPatente(){
        return patente;
    }
    
    public void setMarca(String marca){
        this.marca=marca;
    }
    
    public String getMarca(){
        return marca;
    }
    
    
    public void setTipodeVehiculo(String tipoDeVehiculo){
        this.tipoDeVehiculo=tipoDeVehiculo;
    }
    
    public String getTipodeVehiculo(){
        return tipoDeVehiculo;
    }
    

}
