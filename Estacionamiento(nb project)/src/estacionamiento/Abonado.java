package estacionamiento;

import Exceptions.SaldoInsuficienteException;
import Exceptions.VehiculoNoEncontradoException;
import java.util.ArrayList;


public class Abonado extends Persona{
    private Integer saldo;
    private ArrayList<Vehiculo> vehiculos=new ArrayList<Vehiculo>();
    private Integer ultMonto;
    
    public Abonado(){}

    public Abonado(String nombre, String documento, String apellido, Integer saldo){
        super(nombre, documento, apellido);
        this.saldo=saldo;
    }

    public Integer getUltMonto() {
        return ultMonto;
    }

    public void setUltMonto(Integer ultMonto) {
        this.ultMonto = ultMonto;
    }

    
    public Integer getSaldo() {
        return saldo;
    }

   
    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    
    
    public Integer debitar(Integer monto)throws SaldoInsuficienteException{// Descuenta el monto a cobrar del saldo total. Si no hay saldo suficiente, devuelve una excepcion
        if (saldo-monto<0){
            Integer resto=saldo-monto;
            throw new SaldoInsuficienteException();
        }
        else{
            saldo=saldo-monto;
        }     
        return saldo;
    }
    
    public Integer cargarSaldo(Integer importe){
        saldo=saldo+importe;
        return saldo;
    }    

    /**
     * @return the vehiculos
     */
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    /**
     * @param vehiculos the vehiculos to set
     */
    public void setVehiculos(Vehiculo v) {
        vehiculos.add(v);
    }
    
    public void verificarExistenciaVehiculo(String matricula) throws VehiculoNoEncontradoException{
        int bandera = 0;
        for (Vehiculo vehi : this.getVehiculos()) {
            if (vehi.getPatente().equals(matricula)) {
                bandera = 1;
                break;
            }
        }
        if (bandera == 0) {
            throw new VehiculoNoEncontradoException();
        }
    }
    
    public Vehiculo buscarVehiculo(String matricula) throws VehiculoNoEncontradoException{
        Vehiculo vehi=null;
        int bandera=0;
        for (Vehiculo ve : this.getVehiculos()){
            if(ve.getPatente().equals(matricula)){
                vehi=ve;
                bandera=1;
                break;
            }
        }
        if (bandera==1){
            return vehi;
        }
        else{
            throw new VehiculoNoEncontradoException();
        }
    }
}
