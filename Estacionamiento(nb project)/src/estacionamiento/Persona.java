/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamiento;


public class Persona {
    private String nombre;
    private String documento;
    private String apellido;
    
    public Persona(){}

    public Persona(String nombre, String documento, String apellido) {
        this.nombre = nombre;
        this.documento = documento;
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

   
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    public String getDocumento() {
        return documento;
    }

   
    public void setDocumento(String documento) {
        this.documento = documento;
    }

   
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
}
