
package estacionamiento;
import Exceptions.PlayaDeEstacionamientoLlenaException;
import Interfaz.Main;


public class Estacionamiento {

    
    public static void main(String[] args) throws PlayaDeEstacionamientoLlenaException {//desde aqui se da inicio al programa
        
        Vehiculo auto1=new Vehiculo("ZXC321","Volkswagen","Auto",200);
        Vehiculo auto2=new Vehiculo("AER321","Renault","Auto",200);
        
        
        Principal principal=new Principal();
        Vehiculo vehiculo = new Vehiculo();
        Usuario usuario=new Usuario("Gabriel","87654321","Sigampa","ide-3265",0);
        Cliente cliente=new Cliente();
        Abonado abonado=new Abonado("Rodrigo","40724574","Cordoba",5000);
        abonado.setVehiculos(auto2);
        //principal.setClientes(cliente);
       
        principal.setUsuario(usuario);
        principal.setAbonado(abonado);
        principal.setVehiculos(auto1);
        //principal.setVehiculos(auto2);
        /*metodos
        cargarCliente
        verificarCliente
        verificarVehiculos
        crearTicket
        crearComprobante
        */
        Main main=new Main(principal);
        main.setVisible(true);
        
    }
    
}
