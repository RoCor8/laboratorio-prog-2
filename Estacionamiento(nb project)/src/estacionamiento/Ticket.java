/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamiento;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class Ticket {
    private Cliente cliente;
    private Abonado abono;
    private LocalDate fechaDeTransaccion;
    private LocalDateTime horaDeTransaccion;
    private Integer montoCobrado;

    public Ticket(Cliente cliente , Abonado abono , LocalDate fechaDeTransaccion , LocalDateTime horaDeTransaccion , Integer montoCobrado)
    {
        this.cliente=cliente;
        this.abono=abono;
        this.fechaDeTransaccion=fechaDeTransaccion;
        this.horaDeTransaccion=horaDeTransaccion;
        this.montoCobrado=montoCobrado;
    }

    public Abonado getAbono() {
        return abono;
    }

    public Integer getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Integer montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public LocalDateTime getHoraDeTransaccion() {
        return horaDeTransaccion;
    }

    public void setHoraDeTransaccion(LocalDateTime horaDeTransaccion) {
        this.horaDeTransaccion = horaDeTransaccion;
    }

    public LocalDate getFechaDeTransaccion() {
        return fechaDeTransaccion;
    }

    public void setFechaDeTransaccion(LocalDate fechaDeTransaccion) {
        this.fechaDeTransaccion = fechaDeTransaccion;
    }

    public void setAbono(Abonado abono) {
        this.abono = abono;
    }
}
