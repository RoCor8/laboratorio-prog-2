
package estacionamiento;

import java.util.ArrayList;


public class Cliente extends Persona {
    private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

    public Cliente(){}
    
    public Cliente(String nombre, String documento, String apellido) {
        super(nombre, documento, apellido);
    }
    
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Vehiculo v) {
        vehiculos.add(v);
    }
    

}
