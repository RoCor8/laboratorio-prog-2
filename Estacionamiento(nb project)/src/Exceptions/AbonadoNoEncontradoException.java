
package Exceptions;


public class AbonadoNoEncontradoException extends Exception{
    public AbonadoNoEncontradoException(String mensaje){
        super(mensaje);
    }
    public AbonadoNoEncontradoException(){}
}
