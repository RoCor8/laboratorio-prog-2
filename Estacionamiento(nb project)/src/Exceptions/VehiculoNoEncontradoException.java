
package Exceptions;


public class VehiculoNoEncontradoException extends Exception{
    public VehiculoNoEncontradoException(String mensaje){
        super(mensaje);
    }
    public VehiculoNoEncontradoException(){}
}
