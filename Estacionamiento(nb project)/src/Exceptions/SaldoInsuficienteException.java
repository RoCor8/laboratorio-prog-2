
package Exceptions;


public class SaldoInsuficienteException extends Exception{
    public SaldoInsuficienteException(String mensaje){
        super(mensaje);
    }
    public SaldoInsuficienteException(){
    }
}
