/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Exceptions.AbonadoNoEncontradoException;
import Exceptions.PlayaDeEstacionamientoLlenaException;
import Exceptions.SaldoInsuficienteException;
import Exceptions.VehiculoNoEncontradoException;
import Exceptions.VerificarRepetidosException;
import estacionamiento.Abonado;
import estacionamiento.Cliente;
import estacionamiento.Principal;
import estacionamiento.Vehiculo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class LaboTest {
    
    public LaboTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void testAgregarVehiculos() throws PlayaDeEstacionamientoLlenaException, VehiculoNoEncontradoException{
        System.out.println("Verificar si se agregan vehiculos");
        Principal principal = new Principal();
        Vehiculo aut=new Vehiculo("ZXC321","Volkswagen","Auto",200);
        String matricula="ZXC321";
        principal.setVehiculos(aut);
        Vehiculo vehicle=principal.buscarVehiculoEnPlaya(matricula);
        assertEquals(aut, vehicle);
    }
    
    
     @Test
     public void testBuscarAbonado() throws AbonadoNoEncontradoException{
         Principal principal = new Principal();
         Abonado abonado= new Abonado("Nelson","98765432","Romero",0); 
         principal.setAbonado(abonado);
         String dni=abonado.getDocumento();
         Abonado ab=principal.buscarAbonados(dni);
         assertEquals(abonado, ab);
         
     }
    
    @Test(expected=SaldoInsuficienteException.class)
     public void debitarCuentaSinSaldo() throws SaldoInsuficienteException {
         Abonado abonado= new Abonado("Nelson","98765432","Romero",0);         
         abonado.cargarSaldo(100);
         abonado.debitar(200);
     }
     
     @Test(expected=AbonadoNoEncontradoException.class)
     public void buscarAbonadoNoExistente() throws AbonadoNoEncontradoException{
         Abonado abonado= new Abonado("Nelson","98765432","Romero",0);
         Principal principal = new Principal();
         principal.setAbonado(abonado);
         String doc="12345678";
         principal.verificarExistenciaAbonado(doc);
         }
     
     @Test(expected=VerificarRepetidosException.class)
    public void clienteYaExistente() throws VerificarRepetidosException{
         Cliente cliente1 = new Cliente("Nelson","98765432","Romero");
         Principal principal = new Principal();
         principal.setClientes(cliente1);
         String dni="98765432";
         principal.buscarRepetido(dni);
    }
    
    @Test(expected=PlayaDeEstacionamientoLlenaException.class)
     public void playaLlena()throws PlayaDeEstacionamientoLlenaException{
         Principal principal = new Principal();
         Vehiculo aut1=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut2=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut3=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut4=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut5=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut6=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut7=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut8=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut9=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut10=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut11=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut12=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut13=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut14=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut15=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut16=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut17=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut18=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut19=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut20=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut21=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut22=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut23=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut24=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut25=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut26=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut27=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut28=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut29=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut30=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         Vehiculo aut31=new Vehiculo("ZXC321","Volkswagen","Auto",200);
         principal.setVehiculos(aut1);
         principal.setVehiculos(aut2);
         principal.setVehiculos(aut3);
         principal.setVehiculos(aut4);
         principal.setVehiculos(aut5);
         principal.setVehiculos(aut6);
         principal.setVehiculos(aut7);
         principal.setVehiculos(aut8);
         principal.setVehiculos(aut9);
         principal.setVehiculos(aut10);
         principal.setVehiculos(aut11);
         principal.setVehiculos(aut12);
         principal.setVehiculos(aut13);
         principal.setVehiculos(aut14);
         principal.setVehiculos(aut15);
         principal.setVehiculos(aut16);
         principal.setVehiculos(aut17);
         principal.setVehiculos(aut18);
         principal.setVehiculos(aut19);
         principal.setVehiculos(aut20);
         principal.setVehiculos(aut21);
         principal.setVehiculos(aut22);
         principal.setVehiculos(aut23);
         principal.setVehiculos(aut24);
         principal.setVehiculos(aut25);
         principal.setVehiculos(aut26);
         principal.setVehiculos(aut27);
         principal.setVehiculos(aut28);
         principal.setVehiculos(aut29);
         principal.setVehiculos(aut30);
         principal.setVehiculos(aut31);
     }
     
     
}

     

