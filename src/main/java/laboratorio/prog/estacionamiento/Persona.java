package laboratorio.prog.estacionamiento;

public abstract class Persona {
    private String nombre;
    private String apellido;
    private Integer dni;
    
    public Persona(String nombre, String apellido, Integer dni){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public void setApellido(String apellido){
        this.apellido=apellido;
    }

    public String getApellido(){
        return apellido;
    }

    public void setDni(Integer dni){
        this.dni=dni;
    }

    public Integer getDni(){
        return dni;
    }
}