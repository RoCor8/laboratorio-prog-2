package laboratorio.prog.estacionamiento;

public class Motocicleta extends Vehiculo{
    
    private float tarifa;

    public Motocicleta(String patente, String marca, int modelo, String tipoDeVehiculo, float tarifa){
        super(patente, marca, modelo, tipoDeVehiculo);
        this.tarifa=tarifa;
    }

    public float getTarifa(){
        return tarifa;
    }

    public void setTarifa(float tarifa){
        this.tarifa=tarifa;
    }
}