package laboratorio.prog.estacionamiento;

public abstract class Vehiculo{
    private String patente;
    private String marca;
    private int modelo;
    private String tipoDeVehiculo;

    public Vehiculo(String patente,String marca,int modelo,String tipoDeVehiculo){
        this.patente=patente;
        this.marca=marca;
        this.modelo=modelo;
        this.tipoDeVehiculo=tipoDeVehiculo;
    }

    public void setPatente(String patente){
        this.patente=patente;
    }
    public void setMarca(String marca){
        this.marca=marca;
    }
    public void setModelo(int modelo){
        this.modelo=modelo;
    }
    public void setTipodeVehiculo(String tipoDeVehiculo){
        this.tipoDeVehiculo=tipoDeVehiculo;
    }

}