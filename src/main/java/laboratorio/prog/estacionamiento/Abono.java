package laboratorio.prog.estacionamiento;

public class Abono {

    private Cliente cliente;
    private Integer monto;

    public Abono(Cliente cliente , Integer monto)
    {
        this.cliente=cliente;
        this.monto=monto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}