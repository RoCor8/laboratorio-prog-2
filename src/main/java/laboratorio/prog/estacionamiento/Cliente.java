package laboratorio.prog.estacionamiento;

import java.util.ArrayList;

public class Cliente extends Persona {
    private Abono abono;
    private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
    private Integer ingreso;

    public Cliente(Abono abono, Integer ingreso, String nombre, String apellido, Integer dni) {
        super(nombre, apellido, dni);
        this.abono=abono;
        this.ingreso=ingreso;
    }

    public Integer getIngreso() {
        return ingreso;
    }

    public void setIngreso(Integer ingreso) {
        this.ingreso = ingreso;
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(ArrayList<Vehiculo> vehiculos){
        this.vehiculos = vehiculos;
    }

    public Abono getAbono() {
        return abono;
    }

    public void setAbono(Abono abono) {
        this.abono = abono;
    }

}