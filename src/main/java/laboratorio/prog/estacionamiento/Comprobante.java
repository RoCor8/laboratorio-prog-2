package laboratorio.prog.estacionamiento;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Comprobante {
    private Cliente cliente;
    private Integer montoCobrado;
    private LocalDate fechaDeIngreso;
    private LocalDateTime horaDeIngreso;

    public Comprobante(Cliente cliente , Integer montoCobrado , LocalDate fechaDeIngreso , LocalDateTime horaDeIngreso)
    {
        this.cliente=cliente;
        this.montoCobrado=montoCobrado;
        this.fechaDeIngreso=fechaDeIngreso;
        this.horaDeIngreso=horaDeIngreso;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public LocalDateTime getHoraDeIngreso() {
        return horaDeIngreso;
    }

    public void setHoraDeIngreso(LocalDateTime horaDeIngreso) {
        this.horaDeIngreso = horaDeIngreso;
    }

    public LocalDate getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(LocalDate fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }

    public Integer getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(Integer montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}

/*dominio del
vehículo, apellido y nombre del dueño del vehículo, el valor del ingreso, la fecha de
ingreso y el saldo disponible. También se informa el número de ingreso del día. */