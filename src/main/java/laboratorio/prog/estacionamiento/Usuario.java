package laboratorio.prog.estacionamiento;

public class Usuario extends Persona {
    private String identificacion;

    public Usuario(String nombre, String apellido, Integer dni, String identificacion) {
        super(nombre, apellido, dni);
        this.identificacion=identificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    
}